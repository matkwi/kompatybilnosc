import platform
import pandas as pd
import os
import numpy as np
import obliczenia as ob


def load_dataset(filepath, cols):
    df = pd.read_csv(filepath)
    df.columns = cols
    return df

def generate_list():
    os_name = platform.system()
    cwd = os.getcwd()

    if os_name == "Darwin":
        cwd1 = cwd + '/dziesiec.csv'
        cwd2 = cwd + '/piecdziesiat.csv'
    else:
        cwd1 = "E:\ksr\kompatybilnosc\obliczenia\dziesiec.csv"
        cwd2 = "E:\ksr\kompatybilnosc\obliczenia\piecdziesiat.csv"

    columns = ['distance', '600height20', '600height37,5', '2000height20', '2000height37,5']
    data10 = load_dataset(cwd1, columns)
    data50 = load_dataset(cwd2, columns)

    dataset10 = [[0 for x in range(len(data10))] for y in range(5)]
    dataset50 = [[0 for x in range(len(data50))] for y in range(5)]

    for i in range(len(dataset10)):
        for j in range(len(dataset10[i])):
            if i == 0:
                dataset10[i][j] = data10['distance'][j]
                dataset50[i][j] = data50['distance'][j]
            if i == 1:
                dataset10[i][j] = data10['600height20'][j]
                dataset50[i][j] = data50['600height20'][j]
            if i == 2:
                dataset10[i][j] = data10['600height37,5'][j]
                dataset50[i][j] = data50['600height37,5'][j]
            if i == 3:
                dataset10[i][j] = data10['2000height20'][j]
                dataset50[i][j] = data50['2000height20'][j]
            if i == 4:
                dataset10[i][j] = data10['2000height37,5'][j]
                dataset50[i][j] = data50['2000height37,5'][j]


    dataset10 = np.array(dataset10)
    dataset50 = np.array(dataset50)

    dataset10 = dataset10.transpose()
    dataset50 = dataset50.transpose()

    return dataset50, dataset10