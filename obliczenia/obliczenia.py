import math as m
import pandas as pd
import numpy as np


def calculate_A(dataset10, dataset50):
    A = 15
    # for i in range(1, 10):
    #     for j in range(10):
    #         x = float(str(i) + "." + str(j))
    #         if j == 0:
    #             x = int(x)
            # print (x)  
    base_d = []
    interf_d = []
    alfa_d = []
    diff_d = []

    for alfa in range (0, 190, 10):
        D = 11
        d = 3
        while True:
            x = round(m.sqrt(d**2 + D**2 - (2 * D * d * m.cos(round(m.radians(alfa), 3) ) ) ), 3)
            diff = calculate_distances(d, x, dataset50, dataset10)
            if diff >= 15 and diff <= 15.4:
                break
            elif diff <= 15:
                d -= 0.05
                if d <= 1:
                    break
            elif diff >= 15.4:
                d += 0.05
                if d >= 11:
                    break
            #print("d:", d, "x:", x, "alfa:", alfa, "różnica:", diff)

        base_d.append(round(d, 3))
        interf_d.append(x)
        alfa_d.append (alfa)
        diff_d.append(diff) 
    csv_list = []
    csv_list.append(base_d)
    csv_list.append(interf_d)
    csv_list.append(alfa_d)
    csv_list.append(diff_d)
    csv_list = np.array(csv_list)
    csv_list = csv_list.transpose()
    print(csv_list)
    df = pd.DataFrame(csv_list, columns=["base_d", "interf_d", "alfa_d", "diff_d"])
    df.to_csv('wyniki.csv', index=False)

    

def calculate_distances(d_1, d_2, dataset50, dataset10):
    i = 0
    delta_perp = 5.9128
    f = 1800
    f_inf = 600
    f_sup = 2000
    K = 3.2 + 6.2 * m.log10(f)
    h_1 = 29
    h_2 = 1.8
    h_inf = 20
    h_sup = 37.5
    E = []

    while i < 2:
        if i == 0:
            d = d_1
            A = dataset50

        if i == 1: 
            d = d_2
            A = dataset10

        if d > 20:
            d_inf = 20
            d_sup = 25
            d_ix = 20
        else:
            d_ix = m.floor(d)
            d_inf = m.floor(d)
            d_sup = m.ceil(d)           

        if d - int(d) == 0:
            d = int(d)

        if isinstance(d, float) or d > 20:
            e_inf_600 = A[d_ix-1][1] + (A[d_ix][1] - A[d_ix-1][1]) * m.log10(d/d_inf)/m.log10(d_sup/d_inf)
            e_sup_600 = A[d_ix-1][2] + (A[d_ix][2] - A[d_ix-1][2]) * m.log10(d/d_inf)/m.log10(d_sup/d_inf)
            e_inf_2000 = A[d_ix-1][3] + (A[d_ix][3] - A[d_ix-1][3]) * m.log10(d/d_inf)/m.log10(d_sup/d_inf)
            e_sup_2000 = A[d_ix-1][4] + (A[d_ix][4] - A[d_ix-1][4]) * m.log10(d/d_inf)/m.log10(d_sup/d_inf)
        else:
            e_inf_600 = A[d-1][1]
            e_sup_600 = A[d-1][2]
            e_inf_2000 = A[d-1][3]
            e_sup_2000 = A[d-1][4]
            
        e_600 = e_inf_600 + (e_sup_600 - e_inf_600) * m.log10(h_1/h_inf)/m.log10(h_sup/h_inf) 
        e_2000 = e_inf_2000 + (e_sup_2000 - e_inf_2000) * m.log10(h_1/h_inf)/m.log10(h_sup/h_inf)

        e_600_kor = e_600 + delta_perp + K * m.log10(h_2/10)
        e_2000_kor = e_2000 + delta_perp + K * m.log10(h_2/10)
        Ezak = e_600_kor + (e_2000_kor - e_600_kor)* m.log10(f/f_inf)/m.log10(f_sup/f_inf)
        E.append(round(Ezak, 3))
        i += 1
    diff = E[0] - E[1]
    return round(diff, 3)
